class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = async () => {
    await this.load();
    this.clear();
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.classList.add("col-12", "col-md-6", "col-lg-4");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    // get value variable with getelementbyid
    this.find_capacity = document.getElementById("form_capacity").value;
    this.find_availableAt_date = document.getElementById("form_date").value;
    this.find_availableAt_time = document.getElementById("form_time").value;

    // change date and time to be epochtime
    const datetime = new Date(`${this.find_availableAt_date} ${this.find_availableAt_time.substr(0, 5)}`)
    const beforeEpochTime = datetime.getTime();

    const cars = await Binar.listCars((item) => {
      // Conditional (Ternary) Operator to filter capacity
      const capacity_filter = this.find_capacity > 0 ? item.capacity >= this.find_capacity : true;

      // filter date time 
      const itemDate = new Date(item.availableAt);
      const date_filter = itemDate.getTime() <= beforeEpochTime;

      return capacity_filter && date_filter;
    });

    if (cars != "") {
      document.getElementById("empty").innerHTML = "";
    } else {
      document.getElementById("empty").innerHTML = "Not Available";
      console.log("Car: ", cars);
    }
    Car.init(cars);
  }

  // clear all car list 
  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}