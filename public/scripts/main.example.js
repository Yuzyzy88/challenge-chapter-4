/*
 * Contoh kode untuk membaca query parameter,
 * Siapa tau relevan! :)
 * */

const urlSearchParams = new URLSearchParams(window.location.search);
const params = Object.fromEntries(urlSearchParams.entries());

// Coba olah data ini hehe :)
console.log(params);

/*
 * Contoh penggunaan DOM di dalam class
 * */
const app = new App();
app.loadButton.onclick = app.load();
const refresh = app.loadButton;
refresh.addEventListener('click', function (e) {
    console.log("capacity: ", app.find_capacity);
    console.log("Date: ", app.find_availableAt_date);
    console.log("Time: ", app.find_availableAt_time);
});


app.init().then(app.run);
